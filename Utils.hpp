#ifndef _UTILS_H_

#define _UTILS_H_

#include <string>
#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

string convertInt(int number);

vector<string> &split(const string &s, char delim, vector<string> &elems);

vector<string> split(const string &s, char delim);

#endif /* end of include guard: _UTILS_H_ */
