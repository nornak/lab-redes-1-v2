#ifndef _ENLACE_H_

#define _ENLACE_H_

/* Envía las palabras desde la capa intermedia al otro host
 * y recibe las palabras que llegan desde el otro host y las envía al nivel
 * superior
*/

#include <omnetpp.h>
#include "Trama_enlace_m.h"
#include "EnlaceUtils.hpp"

using namespace std;




class Enlace : public cSimpleModule
{
    private:
        // Tamaño maximo de la ventana
        int tam_ventana;
        // cada cuantas tramas recibidas envia un ACK
        int n_ack;
        // direccion del host actual
        int direccion;
        // la probabilidad de que ocurra un error en el mensaje
        double error;

        // cantidad de tramas enviadas
        int NS;
        
        // cantidad de tramas recibidas
        int NR;

        // tiempo entre cada envio de tramas
        simtime_t t_msj;

        // contador de msj enviados, para controlar la ventana
        int contEnviados;

        // contador usado para seleccionar los elementos del vector
        // Aumenta al enviarse un mensaje.
        // Se vuelve a cero al haber una confirmacion de las tramas (ACK) 
        unsigned int contSeleccion;

        // contador que aumenta al recibir una trama de informacion, para poder
        // respoder con ACK cada 'n_ack'
        int cont_tramasRecibidas;

        // debug para poder tener control sobre los errores
        int cont_debug_error;

        // para indicar cuando esta bloqueado debido a un error en las tramas
        // al estar bloqueado descartas las tramas que llegan (REJ)
        bool bloqueado;

        // Indica el tipo de conexion que tiene
        modoConexion modo_conexion;
        // Indica el tipo de conexion que se quiere establecer
        modoConexion modo_peticion;

        // almacena los mensajes por enviar
        vector<Trama_enlace *> tramasPorEnviar;

        // guarda una copia de las tramas
        vector<Trama_enlace *> bufferAux;
        

    protected:

        virtual void initialize();
        
        virtual void handleMessage(cMessage *msg);


        /*******************
         * Se hace algo con los mensaje recibidos desde arriba, el nivel
         * intermedio.
         * Si no hay conexion se establece.
         * Se encapsula el mensje recibido desde el intermedio en una nueva trama.
         * Se guarda el mensaje creado en el buffer de tramas por enviar
         * @param msg - puntero del mensaje que se recibe
         ******************/
        void recibidoDeIntermedio(cPacket *msg);


        /*******************
         * Se hace algo con los mensajes programados recibidos
         * @param msg - puntero del mensaje que se recibe
         ******************/
        void recibidoProgramado(Trama_enlace *msg);


        /*******************
         * Se hace algo con los mensajes recibidos desde el nivel fisico, del
         * otro host.
         * Se separa en TRAMA_I, TRAMA_S y TRAMA_U
         * @param msg - puntero del mensaje que se recibe desde el otro host
         ******************/
        void recibidoDeFisico(Trama_enlace *msg);

        /*******************
         * Crea y envia la trama para esablecer la conexion
         ******************/
        void establecerConexion();

        /*******************
        * Procesa los mensajes que son del tipo informacion
        * Se saca el mensaje que viene encapsulado y se envia hacia arriba (intermedio).
        * En caso de que sea necesario se responde con un RR
        * @param msg - trama que llega del otro host
        ******************/
        void procesarI(Trama_enlace * msg);

        /*******************
         *
        * Procesa los mensaje que son del tipo informacion y llegaron con error
        * @param msg - trama que llega del otro host
        ******************/
        void procesarIError(Trama_enlace * msg);

        /*******************
        * Procesa los mensajes que vienen del otro host y son del tipo
        * supervisora
        * Si llega un mensaje del tipo RR se libera la ventana
        * @param msg - trama que llega del otro host
        ******************/
        void procesarS(Trama_enlace * msg);

        /*******************
        * Procesa los mensajes que vienen del otro host y son del tipo
        * no-numerada.
        * Si llega un mensaje solicitando conexion se envia una respuesta de
        * confirmacion.
        * Si llega un mensaje de confirmacion, se confirma la conexion con el otro host.
        * @param msg - trama que llega del otro host
        ******************/
        void procesarU(Trama_enlace * msg);

        /*******************
         * Verifica si hay tramas en el vector 'tramasPorEnviar', si las hay,
         * envia una de ellas, la primera en el vector.
         * idea: Se eliminan cuando se recibe el ACK del otro host.
         * idea: en caso de error se tienen guardados las tramas
         * idea: Habia que hacer unos contadores o enteros para indicar cual se
         * envio
         ******************/
        void enviarTrama();

        /*******************
         * Retorna true si se puede enviar
         * Dependera de muchas variables, tamanio de la ventana, si se esta
         * estableciendo la comunicacion
         ******************/
        bool puedeEnviar();
        /*******************
         * se el mensjase llega bien o con error
         *
         ******************/
        bool llegoOK();
        void destruirTramas(vector<Trama_enlace*> &v, int cantidad);
};






#endif /* end of include guard: _ENLACE_H_ */
