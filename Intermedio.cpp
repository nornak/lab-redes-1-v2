/*
* Archivo que controla el nivel intermedio
*/

#include <Intermedio.hpp>

Define_Module( Intermedio );


void Intermedio::handleMessage(cMessage *msg)
{
    if (msg->arrivedOn("recibir_abajo"))
    {
        send(msg, "enviar_arriba");
    }else{
        send(msg, "enviar_abajo");
    }
}

