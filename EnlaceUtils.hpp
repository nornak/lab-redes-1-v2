#ifndef _ENLACEUTILS_H_

#define _ENLACEUTILS_H_

#include <omnetpp.h>
#include "Trama_enlace_m.h"
#include "Utils.hpp"

using namespace std;

enum TramaTipo
{
    TRAMA_I = 1,    // verde
    TRAMA_S = 4,    // amarillo
    TRAMA_U = 2    // azul
};

// los modos de la trama del tipo no-numerada
enum tipo_u
{
    // modos de conexion
    SNRM = 0,
    SNRME,
    SARM,
    SARME,
    SABM,
    SABME,

    UI,    // unnumbered information:  informacion no-numerada    -> P/F
    UA,    // unnumbered acknowledged: confirmacion no-numerada   -> F
    DISC,  // solicitud de desconexion, se responde con UA        -> P
    DM,    // disconnect mode: respuesta a DISC                   -> F
    RD,    // request disconnect: solicitud de desconexion (DISC) -> F
    SIM,   // set initialization mode: Initialize link control function in the addressed station    -> P
    RIM,   // Request Initialization Mode: inicializacion necesaria, solicitude de SIM              -> F
    UP,    // Unnumbered Poll: se utiliza para solicitar informacion de control                     -> P
    RSET,  // reset: sirve para reiniciar la conexion, se pone a cero los contadores y las ventanas deslizantes
    XID,   // Exchange Identification: se utiliza para el intercambio de capacidades de los terminales  -> P/F
    TEST,  // test: is simply a ping command for debugging purposes                                     -> P/F                                      
    FRMR,  // Frame Reject: Reporte de la recepción de marco inaceptable                                -> F
    NR0,   // Nonreserved 0: no estanderizado
    NR1,   // Nonreserved 1: no estanderizado
    NR2,   // Nonreserved 2: no estanderizado
    NR3    // Nonreserved 3: no estanderizado
};

enum modoConexion
{
    S_C,    // sin conexion
    NRM,    // modo de respuesta normal
    NRME,   // modo de respuesta normal extendido
    ABM,    // modo balanceado Asincrono
    ABME,   // modo balanceado Asincrono extendido
    ARM,    // modo de respuesta Asincrono
    ARME    // modo de respuesta Asincrono extendido
};

// los modos de la trama supervisora
enum tipo_s
{
    RR = 0, // si la primaria envia con P, indica que esta pidienfo informacion de la segunda estacion
            // si esta el bit F, es una respuesta de la segunda estacion hacia la primera
    REJ,
    RNR,
    SREJ
};

enum bitPF 
{
    B_NO,
    B_P,
    B_F
};

/*******************
 *
 * Funciones
 *
 ******************/

/*******************
 * Crea una trama, esta es la mas simple
 * @param msg - el mensaje que se mostrara en la ejecucion
 * @param tipo - un entero que indica el tipo de la trama, puede ser: TRAMA_I,
 * TRAMA_S y TRAMA_U
 * @return - puntero de la nueva trama
 ******************/
Trama_enlace * crearTrama(const char *msg, int tipo);


/*******************
 * Crea una trama del tipo de informacion. Agrega el sgte texto: "I, NS, NR"
 * al puntito que se muestra en la interfaz
 * @param NS - la cantidad de tramas enviadas
 * @param NR - la cantidad de tramas recibidas
 * @return - puntero de la nueva trama creada
 ******************/
Trama_enlace * crearTramaI(int NS, int NR);



/*******************
 * Crea una trama del tipo de supervisora. Agrega el sgte texto: RR NR
 * al puntito que se muestra en la interfaz
 * @param texto - el texto que se mostrara
 * @param tipo - tipo de la tramas S
 * @param NR - la cantidad de tramas recibidas
 * @param bit - presencia o ausencia del bit PF, recibe: B_P (bit P), B_F (bit F)
 * y B_NO (sin el bit)
 * @return - puntero de la nueva trama creada
 ******************/
Trama_enlace * crearTramaS(const char *texto, tipo_s tipo, int NR, bitPF bit);



/*******************
 * Crea una trama del tipo nonumerada, para la iniciacion de la conexion, se le
 * da el tipo de conexion y retorna un puntero de la trama creada, con el tipo
 * expecificado
 * al puntito que se muestra en la interfaz
 * @param tipo - el tipo de conexion que se quiere establecer
 * @return - puntero de la nueva trama creada
 ******************/
Trama_enlace * crearTramaInicioTransmicion(modoConexion tipo);

    



/*******************
 * Dado una trama tipo no-numerada obtiene el tipo de conexion que quiere
 * establecer.
 * retorna el valor del tipo de conexion.
 * @param trama - la trama del tipo U que se quiere sacar el modo
 * @return - el modo que quiere establecer la trama
 ******************/
modoConexion obtenerModo(Trama_enlace *trama);


/*******************
 * Cambia los valores de NS y NR de la trama.
 * Tanto los valores de la trama, como el mensaje que muestra
 * @param trama - la trama a la cual se le cambiaran los valores y el nombre que
 * muestra
 * @param NS - el nuevo valor de NS
 * @param NR - el nuevo valor de NR
 ******************/
void cambiarNS_NR(Trama_enlace *trama, int NS, int NR);

#endif /* end of include guard: _ENLACEUTILS_H_ */
