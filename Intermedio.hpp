#ifndef _INTERMEDIO_H_

#define _INTERMEDIO_H_
/*
* Archivo que controla el nivel intermedio
*/

#include <omnetpp.h>

//Nombre de la clase y tipo
class Intermedio : public cSimpleModule
{
    //Métodos
    protected:
        //Receptor de mensajes
        virtual void handleMessage(cMessage *msg);
};





#endif /* end of include guard: _INTERMEDIO_H_ */
