/*
 * En éste módulo se generan palabras de información, la cual viaja
 * a los modulos inferiores para ser enviada al otro Host a través del canal
 */

#include "Aplicacion.hpp"
#include "Utils.hpp"
#include <sstream>
#include <algorithm>
#include <iterator>

Define_Module( Aplicacion );


//Inicializar
void Aplicacion::initialize()
{
    n_trama = par("n_trama");
    msgPorSeg = 0.01;
    contador = 1;

    if(n_trama != 0)
    {
        string texto = "mensaje ";
        texto += convertInt(contador);
        cPacket *nuevo = new cPacket(texto.c_str());

        send(nuevo, "enviar_abajo");

        n_trama--;
        contador++;

        if(n_trama != 0)
        {
            texto = "APP: programado";
            texto += convertInt(contador);
            cMessage *timeout = new cMessage(texto.c_str());
            scheduleAt(simTime() + msgPorSeg, timeout);
        }
    }
    
}



//Manejar mensajes
void Aplicacion::handleMessage(cMessage *msg)
{
    EV << "En APP -> ";
    if(msg->isSelfMessage())
    {
        EV << "msg programado" << endl;

        string texto = "mensaje ";
        texto += convertInt(contador);
        cPacket *nuevo = new cPacket(texto.c_str());

        send(nuevo, "enviar_abajo");

        n_trama--;
        contador++;

        if(n_trama != 0)
        {
            texto = "APP: programado";
            texto += convertInt(contador);
            cMessage *timeout = new cMessage(texto.c_str());
            scheduleAt(simTime() + msgPorSeg, timeout);
        }
        delete msg;
    }
    else if(msg->arrivedOn("recibir_abajo"))
    {
        EV << "msg desde intermedio" << endl;
        delete msg;
        
    }


}
