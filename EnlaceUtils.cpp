
#include "EnlaceUtils.hpp" 

/*******************
 * Crea una trama, esta es la mas simple
 * @param msg - el mensaje que se mostrara en la ejecucion
 * @param tipo - un entero que indica el tipo de la trama, puede ser: TRAMA_I,
 * TRAMA_S y TRAMA_U
 * @return - puntero de la nueva trama
 ******************/
Trama_enlace * crearTrama(const char *msg, int tipo)
{
    Trama_enlace * nuevo = new Trama_enlace(msg, tipo);
    nuevo->setTipo(tipo);
    nuevo->setDato("");
    nuevo->setDestino(0);
    nuevo->setMsj(0);
    nuevo->setBitPF(0);
    nuevo->setNS(0);
    nuevo->setNR(0);
    return nuevo;
}



/*******************
 * Crea una trama del tipo de informacion. Agrega el sgte texto: "I, NS, NR"
 * al puntito que se muestra en la interfaz
 * @param NS - la cantidad de tramas enviadas
 * @param NR - la cantidad de tramas recibidas
 * @return - puntero de la nueva trama creada
 ******************/
Trama_enlace * crearTramaI(int NS, int NR)
{

    string nombre = "I,";
    nombre += convertInt(NS);
    nombre += ",";
    nombre += convertInt(NR);
    Trama_enlace * nuevo = crearTrama(nombre.c_str(), TRAMA_I);
    nuevo->setNS(NS);
    nuevo->setNR(NR);
    return nuevo;
}



/*******************
 * Crea una trama del tipo de supervisora. Agrega el sgte texto: RR NR
 * al puntito que se muestra en la interfaz
 * @param texto - el texto que se mostrara
 * @param tipo - tipo de la tramas S
 * @param NR - la cantidad de tramas recibidas
 * @param bit - presencia o ausencia del bit PF, recibe: B_P (bit P), B_F (bit F)
 * y B_NO (sin el bit)
 * @return - puntero de la nueva trama creada
 ******************/
Trama_enlace * crearTramaS(const char *texto, tipo_s tipo, int NR, bitPF bit)
{

    string nombre(texto);
    nombre += " ";
    nombre += convertInt(NR);
    Trama_enlace * nuevo = NULL;

    if(bit == B_F)
    {
        nombre += " F";
        nuevo = crearTrama(nombre.c_str(), TRAMA_S);
        nuevo->setBitPF(B_F);

    }
    else if(bit == B_P)
    {
        nombre += " P";
        nuevo = crearTrama(nombre.c_str(), TRAMA_S);
        nuevo->setBitPF(B_P);

    }else{
        nuevo = crearTrama(nombre.c_str(), TRAMA_S);
    }

    if(tipo == RR)
    {
        nuevo->setMsj(RR);
    }
    else if(tipo == REJ)
    {
        nuevo->setMsj(REJ);
    }

    nuevo->setNS(0);
    nuevo->setNR(NR);

    return nuevo;
}


/*******************
 * Crea una trama del tipo nonumerada, para la iniciacion de la conexion, se le
 * da el tipo de conexion y retorna un puntero de la trama creada, con el tipo
 * expecificado
 * al puntito que se muestra en la interfaz
 * @param tipo - el tipo de conexion que se quiere establecer
 * @return - puntero de la nueva trama creada
 ******************/
Trama_enlace * crearTramaInicioTransmicion(modoConexion tipo)
{
    Trama_enlace *sincro = NULL;

    if( tipo == NRM)
    {
        sincro = new Trama_enlace("SNRM P", TRAMA_U);
        sincro->setMsj(SNRM);
        sincro->setTipo(TRAMA_U);
        // bit P
        sincro->setBitPF(1);
    }
    else if (tipo == NRME)
    {
        sincro = new Trama_enlace("SNRME P", TRAMA_U);
        sincro->setMsj(SNRME);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
    }
    else if (tipo == ABM)
    {
        sincro = new Trama_enlace("SABM P", TRAMA_U);
        sincro->setMsj(SABM);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
    }
    else if (tipo == ABME)
    {
        sincro = new Trama_enlace("ABME P", TRAMA_U);
        sincro->setMsj(SABME);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
    }
    else if (tipo == ARM)
    {
        sincro = new Trama_enlace("ARM P", TRAMA_U);
        sincro->setMsj(SARM);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
    }
    else if (tipo == ARME)
    {
        sincro = new Trama_enlace("ARME P", TRAMA_U);
        sincro->setMsj(SARME);
        sincro->setTipo(TRAMA_U);
        sincro->setBitPF(1);
    }

    return sincro;
}



/*******************
 * Dado una trama tipo no-numerada obtiene el tipo de conexion que quiere
 * establecer
 * @param trama - la trama del tipo U que se quiere sacar el modo
 * @return - el modo que quiere establecer la trama
 ******************/
modoConexion obtenerModo(Trama_enlace *trama)
{
    int msj = trama->getMsj();

    if (msj == SNRM)
    {
        return NRM;
    }
    else if (msj == SNRME)
    {
        return NRME;
    }
    else if (msj == SARM)
    {
        return ARM;
    }
    else if (msj == SARME)
    {
        return ARME;
    }
    else if (msj == SABM)
    {
        return ABM;
    }
    else if (msj == SABME)
    {
        return ABME;
    }
    // cuando la trama no es de seteo de conexion
    // es para diferenciar del resto
    return S_C;
}



/*******************
 * Cambia los valores de NS y NR de la trama.
 * Tanto los valores de la trama, como el mensaje que muestra
 * @param trama - la trama a la cual se le cambiaran los valores y el nombre que
 * muestra
 * @param NS - el nuevo valor de NS
 * @param NR - el nuevo valor de NR
 ******************/
void cambiarNS_NR(Trama_enlace *trama, int NS, int NR)
{
    string nombre = "I,";
    nombre += convertInt(NS);
    nombre += ",";
    nombre += convertInt(NR);
    trama->setName(nombre.c_str());
    trama->setNS(NS);
    trama->setNR(NR);
}

