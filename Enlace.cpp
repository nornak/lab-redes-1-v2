/* Envía las palabras desde la capa intermedia al otro host
 * y recibe las palabras que llegan desde el otro host y las envía al nivel
 * superior
*/

#include <Enlace.hpp>

using namespace std;


Define_Module( Enlace );

// TODO hacer el rechazo simple, con todo lo que acarrea, reseteo de
// ventana..etc ->  tratamiento de error
// TODO hacer los timeout, solo se harian los de TRAMA_I


/*******************
 * Inicializador
 ******************/
void Enlace::initialize()
{
    tam_ventana = par("tam_ventana");
    n_ack = par("n_ack");
    direccion = par("direccion");
    error = par("error");

    modo_conexion = S_C;
    modo_peticion = S_C;

    NS = 0;
    NR = 0;

    t_msj = 0.05;

    contEnviados = 0;
    contSeleccion= 0;
    cont_tramasRecibidas = 0;
    cont_debug_error = 0;

    bloqueado = false;

    WATCH_VECTOR(tramasPorEnviar);
    WATCH_VECTOR(bufferAux);
    WATCH(NS);
    WATCH(NR);
}


/*******************
 * Manejador de mensajes
 ******************/
void Enlace::handleMessage(cMessage *msg)
{
    ev << "En Enlace, host: " << direccion << endl;

    // llega mensaje desde intermedio
    if( msg->arrivedOn("recibir_arriba"))
    {
        EV << "recibido de arriba" << endl;
        cPacket *msj = check_and_cast<cPacket *>(msg);
        recibidoDeIntermedio(msj);
    }
    // llega un mensaje programado
    else if( msg->isSelfMessage() )
    {
        EV << "recibido programado ->" ;
        Trama_enlace *msj = check_and_cast<Trama_enlace *>(msg);
        recibidoProgramado(msj);

    }
    // llega un mensaje desde otro host
    else if(msg->arrivedOn("recibir_fisico"))
    {
        EV << "recibido fisico" << endl;
        Trama_enlace *msj = check_and_cast<Trama_enlace *>(msg);
        recibidoDeFisico(msj);
    }
    if(puedeEnviar() and msg->isSelfMessage())
    {
        enviarTrama();
    }
}



/*******************
 * Se hace algo con los mensaje recibidos desde arriba, el nivel
 * intermedio.
 * Si no hay conexion se establece.
 * Se encapsula el mensje recibido desde el intermedio en una nueva trama.
 * Se guarda el mensaje creado en el buffer de tramas por enviar
 * @param msg - puntero del mensaje que se recibe
 ******************/
void Enlace::recibidoDeIntermedio(cPacket *msg)
{
    if(modo_conexion == S_C and modo_peticion == S_C)
    {
        EV << "sin conexion, estableciendo" << endl;
        bubble("estableciendo conexion");

        string texto = msg->getName();
        texto += " E";
        Trama_enlace *nuevo = crearTrama(texto.c_str() , TRAMA_I);

        nuevo->encapsulate(msg);

        // se guarda el mensaje que se iba a enviar
        tramasPorEnviar.push_back(nuevo);
        bufferAux.push_back(new Trama_enlace(*nuevo));

        establecerConexion();

    }else{
        EV << "se guarda el msj ->" ;
        
        string texto = msg->getName();
        texto += " E";
        // los mensajes se guardan
        Trama_enlace *nuevo = crearTrama(texto.c_str() , TRAMA_I);

        nuevo->encapsulate(msg);

        tramasPorEnviar.push_back(nuevo);
        bufferAux.push_back(new Trama_enlace(*nuevo));

        EV << "tamanio vector: " << tramasPorEnviar.size() << endl;
    }
}



/*******************
 * Se hace algo con los mensajes programados recibidos
 * @param msg - puntero del mensaje que se recibe
 ******************/
void Enlace::recibidoProgramado(Trama_enlace *msg)
{
    switch(msg->getTipo())
    {
        case TRAMA_I:
            ev << "TRAMA I" << endl;
            delete msg;
            break;

        case TRAMA_S:
            ev << "TRAMA S" << endl;
            // TODO esto no sera necesario
            break;

        case TRAMA_U:
            ev << "TRAMA U" << endl;
            // TODO esto no sera necesario
            break;
    }
}



/*******************
 * Se hace algo con los mensajes recibidos desde el nivel fisico, del
 * otro host.
 * Se separa en TRAMA_I, TRAMA_S y TRAMA_U
 * @param msg - puntero del mensaje que se recibe desde el otro host
 ******************/
void Enlace::recibidoDeFisico(Trama_enlace *msg)
{
    switch(msg->getTipo())
    {
        case TRAMA_I:
            EV << "Trama I ->";

            if(llegoOK())
            {
                procesarI(msg);
            }else{
                EV << "bloqueado" << endl;
                bloqueado = true;

                cont_tramasRecibidas ++;
                procesarIError(msg);
            }
            cont_debug_error ++;

            break;

        case TRAMA_S:
            EV << "Trama S ->";
            procesarS(msg);
            break;

        case TRAMA_U:
            EV << "Trama U ->";
            procesarU(msg);
            break;
    }
}



/*******************
* Crea y envia la trama para esablecer la conexion
******************/
void Enlace::establecerConexion()
{
    Trama_enlace *nuevo = crearTramaInicioTransmicion(ABM);
    modo_peticion = ABM;

    send(nuevo, "enviar_fisico");
}




/*******************
* Procesa los mensajes que son del tipo informacion
* Se saca el mensaje que viene encapsulado y se envia hacia arriba (intermedio).
* En caso de que sea necesario se responde con un RR
* @param msg - trama que llega del otro host
******************/
void Enlace::procesarI(Trama_enlace * msg)
{
    EV << "procesando TRAMA_I -> ";

    // mensaje con enumeracion erronea
    if(msg->getNS() != NR or bloqueado)
    {
        // se descarta
        EV << "numeracion erronea o bloqueado -> se descarta el mensaje" << endl;
        cont_tramasRecibidas ++;
        // se llama a esta funcion por que hay que hacer lo mismo que si se
        // llegara con un mensaje de error
        procesarIError(msg);
    }
    else{
        EV << "no hay problema con la numeracion" << endl;

        NR ++;
        cont_tramasRecibidas ++;

        if(NR == (tam_ventana + 1))
        {
            NR = 0;
        }
        if( (cont_tramasRecibidas == n_ack) or (msg->getBitPF() == B_P))
        {
            Trama_enlace *respuesta = NULL;
            if(msg->getBitPF() == B_P)
            {
                respuesta = crearTramaS("RR", RR, NR, B_F);
            }else{
                respuesta = crearTramaS("RR", RR,  NR, B_NO);
            }

            send(respuesta, "enviar_fisico");
            cont_tramasRecibidas = 0;
        }

        cPacket *dato = msg->decapsulate();

        send(dato, "enviar_arriba");
        delete msg;
    }

}


/*******************
* Procesa los mensaje que son del tipo informacion y llegaron con error
* @param msg - trama que llega del otro host
******************/
void Enlace::procesarIError( Trama_enlace *msg)
{
    ev << "tramas recibidas: " << cont_tramasRecibidas << endl;
    if(cont_tramasRecibidas == (n_ack) or (msg->getBitPF() == B_P))
    {
        Trama_enlace *respuesta = NULL;
        respuesta = crearTramaS("REJ",REJ, NR, B_NO);

        send(respuesta, "enviar_fisico");
        bloqueado = false;
        cont_tramasRecibidas = 0;
    }
}


/*******************
* Procesa los mensajes que vienen del otro host y son del tipo
* supervisora
* Si llega un mensaje del tipo RR se libera la ventana
* @param msg - trama que llega del otro host
******************/
void Enlace::procesarS(Trama_enlace * msg)
{
    EV << "procesando TRAMA_S -> " ;
    EV << msg->getMsj()  << " -> ";
    // trama S con bit P
    if(msg->getBitPF() == B_P)
    {

    }
    // trama S con bit F
    else if(msg->getBitPF() == B_F)
    {
        EV << "bit F -> " ;
        if(msg->getMsj() == RR)
        {
            EV << "mensaje RR" << endl;
            EV << "contEnviados: " << contEnviados << " -> ";
            // se agrega espacio a la ventana
            contEnviados -= n_ack;
            EV << contEnviados << endl;

            // se eliminan los elementos confirmados del vector
            tramasPorEnviar.erase(tramasPorEnviar.begin(), tramasPorEnviar.begin() + contSeleccion);
            destruirTramas(bufferAux, contSeleccion);
            bufferAux.erase(bufferAux.begin(), bufferAux.begin() + contSeleccion);

            // se resetea el contador que selecciona los elementos enviados del
            // vector 'tramasPorEnviar'
            contSeleccion = 0;
        }
    }
    // un mensaje RR
    else if (msg->getMsj() == RR and msg->getBitPF() == B_NO)
    {
        EV << "mensaje RR" << endl;
        EV << "contEnviados: " << contEnviados << " -> ";
        // se agrega espacio a la ventana
        contEnviados -= n_ack;
        EV << contEnviados << endl;
        EV << "eliminado: " << contSeleccion << endl;

        // se eliminan los elementos confirmados del vector
        tramasPorEnviar.erase(tramasPorEnviar.begin(), tramasPorEnviar.begin() + contSeleccion);
        destruirTramas(bufferAux, contSeleccion);
        bufferAux.erase(bufferAux.begin(), bufferAux.begin() + contSeleccion);
        // se resetea el contador que selecciona los elementos enviados del
        // vector 'tramasPorEnviar'
        contSeleccion = 0;

    }
    else if(msg->getMsj() == REJ)
    {
        int rej = msg->getNR();
        unsigned int i = 0;
        int ns;

        EV << "mensaje REJ" << endl;
        EV << "cantidad restada: " << contSeleccion << endl;


        while(i < contSeleccion)
        {
            ns = tramasPorEnviar[0]->getNS();

            EV << "ns = " << ns << " rej " << rej << endl;
            if( ns == rej)
                break;

            destruirTramas(tramasPorEnviar, 1);
            tramasPorEnviar.erase(tramasPorEnviar.begin());
            destruirTramas(bufferAux, 1);
            bufferAux.erase(bufferAux.begin());

            i++;
        }

        contEnviados -= contSeleccion;
        contSeleccion = 0;
        NS = msg->getNR();
    }
    delete msg;
}


/*******************
* Procesa los mensajes que vienen del otro host y son del tipo
* no-numerada.
* Si llega un mensaje solicitando conexion se envia una respuesta de
* confirmacion.
* Si llega un mensaje de confirmacion, se confirma la conexion con el otro host.
* @param msg - trama que llega del otro host
******************/
void Enlace::procesarU(Trama_enlace * msg)
{
    EV << "procesando TRAMA_U -> " ;

    // si hay solicitud de conexion y no hay una conexion establecida
    if (modo_conexion == S_C and (obtenerModo(msg) != S_C))
    {
        EV << "enviando respuesta " << endl;

        // se establece la conexion, obteniendo el modo del mensaje recibido
        modo_conexion = obtenerModo(msg);

        Trama_enlace *respuesta = crearTrama("UA F", TRAMA_U);
        respuesta->setMsj(UA);
        // tiene bit F
        respuesta->setBitPF(B_F);

        send(respuesta, "enviar_fisico");

    }
    // si llega un mensaje de confirmacion
    else if (msg->getMsj() == UA)
    {
        bubble("conexion confirmada");

        EV << "conexion confirmada" << endl;
        modo_conexion = modo_peticion;

        // se empeiza a enviar mensajes, al recibir el mensaje de confirmacion
        Trama_enlace *programado = crearTrama("programado", TRAMA_I);
        // se prograna en el tiempo actual, para que el msj programado llege al
        // mismo tiempo que fue llamado (la funcion scheduleAt)
        scheduleAt(simTime() , programado);

    }
    delete msg;
}


/*******************
 * Verifica si hay tramas en el vector 'tramasPorEnviar', si las hay,
 * envia una de ellas, la primera en el vector.
 * idea: Se eliminan cuando se recibe el ACK del otro host.
 * idea: en caso de error se tienen guardados las tramas
 * idea: Habia que hacer unos contadores o enteros para indicar cual se
 * envio
 ******************/
void Enlace::enviarTrama()
{
    // si hay tramsa en el buffer se envia
    if( tramasPorEnviar.size() != 0)
    {
        EV << "hay tramas que enviar, size() = " << tramasPorEnviar.size() << endl;
        Trama_enlace *temp = tramasPorEnviar[contSeleccion];

        // a causa de una rej se tienen mensajes guardados en la trama que ya se
        // enviaron, debido a que no se eliminaron por que no se recibio algun
        // RR
        if(temp->getSenderModule() != NULL)
        {
            EV << "ya fue programada" << endl;
            // se reenvia el mensaje. A causa de un REJ
            temp = bufferAux[contSeleccion];

            bufferAux[contSeleccion] = new Trama_enlace(*temp);
        }


        ev << "NR " << NR << " NS " << NS << endl;
        cambiarNS_NR(temp, NS, NR);
        // si se envia el ultimo del vector se pone el bit P, para
        // olbigar a responder al receptor
        if(contSeleccion == (tramasPorEnviar.size() - 1))
        {
            EV << "ultima trama, se agrega bit P" << endl;
            string texto(temp->getName());
            texto += " P";
            temp->setName(texto.c_str());
            temp->setBitPF(B_P);
        }

        ev << "NR " << temp->getNR() << " NS " << temp->getNS() << endl;
        send(temp, "enviar_fisico");

        NS++;
        EV << "contSeleccion = " << contSeleccion <<  " -> ";
        contEnviados++;
        contSeleccion++;
        EV << contSeleccion << endl;


        // si todavia hay espacio en la ventana, se crea un msg programado
        if(contEnviados < tam_ventana)
        {
            EV << "enviados: " << contEnviados << endl;
            string texto = "ENLC: programado ";
            texto += convertInt(NS);
            Trama_enlace *programado = crearTrama(texto.c_str(), TRAMA_I);
            programado->setNS(NS);
            scheduleAt(simTime() + t_msj, programado);
        }
        else{
            EV << "no hay ventana: " << contEnviados << endl;
        }

        // resetea el contador de los mensaje enviados
        // tiene mas 1 para hacer que calze cuando al principio de esta funcion
        // empezaba con 4 (tomando una ventana de 5), se enviaba y se aumenta el
        // valor de NS en 1, osea 5. Sin el +1 se resetea sin poder enviar la
        // trama con NS = 5
        if(NS == (tam_ventana + 1))
        {
            NS = 0;
        }
    }
}

/*******************
 * Retorna true si se puede enviar
 * Dependera de muchas variables, tamanio de la ventana, si se esta
 * estableciendo la comunicacion
 ******************/
bool Enlace::puedeEnviar()
{
    if((modo_conexion != S_C) and (tramasPorEnviar.size() != 0))
    {
        EV << "puede enviar" << endl;
        return true;
    }

    EV << "no puede enviar" << endl;
    return false;

}


/*******************
 * funcion que retorna si el mensaje llega bien o con error
 ******************/
bool Enlace::llegoOK()
{
    if(uniform(0,1) < error )
    {
        EV << "llego con error" << endl; 
        bubble("trama con error");
        return false;
    }else{
        EV << "llego okidoki" << endl;
        return true;
    }
}

void Enlace::destruirTramas(vector<Trama_enlace*> &v, int cantidad)
{
    int i = 0;
    Trama_enlace *temp = NULL;
    for(i = 0; i < cantidad; i++)
    {
        temp = v[i];
        delete temp;
   }
}
