#ifndef _APLICACION_H_

#define _APLICACION_H_
/*
 * En éste módulo se generan palabras de información, la cual viaja
 * a los modulos inferiores para ser enviada al otro Host a través del canal
 */

#include <omnetpp.h>

using namespace std;

//Nombre de la clase y tipo de la librería omnetpp.h
class Aplicacion : public cSimpleModule
{
    private: 
        // cantidad de mensajes que tiene que enviar
        int n_trama;
        // le contador
        int contador;
        // cada cuando se envia otro mensaje, en segundos
        double msgPorSeg;

    public:
        //Inicializar módulo
        virtual void initialize();
        //Manejador de mensajes
        virtual void handleMessage(cMessage *msg);
};




#endif /* end of include guard: APLICACION_H */
